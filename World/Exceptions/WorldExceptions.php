<?php

namespace World\Exceptions;

abstract class WorldExceptions extends \Exception
{
    /**
     * @var $error Maybe anything
     */
    protected $error;

    /**
     * WorldExceptions constructor.
     * @param string $context
     */
    public function __construct($context = null)
    {
        $this->formatError($context);
        parent::__construct($this->error);
    }

    /**
     * @return void formate error message with context
     */
    abstract protected function formatError($context) : void;
}