<?php

namespace World\Effects;

/**
 * Interface EffectInterface
 * @package World\Effects
 *
 * All effects what has give one Entity to another Entity, and this can make effect on target . like 'bit' , 'woof' , 'eat'
 */
interface EffectInterface
{

}