<?php

namespace World\Entity\Inanimate;

use World\Actions\ActionInterface;

abstract class AbstractInanimate implements InanimateInterface
{
    /**
     * @var ActionInterface
     */
    protected $action;

    /**
     * @return ActionInterface
     *
     * entity has some action, or has possibility filter actions
     */
    abstract public function getAction(): ActionInterface;

    /**
     * AbstractEntity constructor.
     * @param ActionInterface $action
     */
    public function __construct(ActionInterface $action)
    {
        $this->action = $action;
    }
}