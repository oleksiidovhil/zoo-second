<?php

namespace World\Entity\Inanimate;

use World\Entity\EntityInterface;

interface InanimateInterface extends EntityInterface
{}