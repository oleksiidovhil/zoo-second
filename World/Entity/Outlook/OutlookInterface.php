<?php

namespace World\Entity\Outlook;

use World\Actions\ActionInterface;
use World\Entity\EntityInterface;

/**
 * Interface OutlookInterface
 * @package World\Entity\Outlook
 */

interface OutlookInterface
{
    /**
     * @param ActionInterface $action
     * @return ActionInterface
     */
    public function delegateAction(ActionInterface $action) : ?ActionInterface;

    /**
     * @param EntityInterface $entity
     * @return mixed
     *
     * Set what sees now animate person.
     * In general this need be collection interrelated entities,
     * but will take a long time
     */
    public function __construct(EntityInterface $entity);

    /**
     * @return string
     */
    public function getEntityPrefix() : string;
}