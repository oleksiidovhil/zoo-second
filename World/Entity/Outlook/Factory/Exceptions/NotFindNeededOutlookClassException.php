<?php

namespace World\Entity\Outlook\Factory\Exceptions;

use World\Entity\EntityInterface;
use World\Exceptions\WorldExceptions;

class NotFindNeededOutlookClassException extends WorldExceptions
{
    protected function formatError($context): void
    {
        if ( !($context instanceof EntityInterface))
        {
            $this->error = 'get value need instanceof EntityInterface';
            return;
        }
        $this->error = 'You not implemented Outlook class for ' . get_class($context) . ' entity';
    }
}