<?php

namespace World\Entity\Outlook\Factory;

use World\Entity\EntityInterface;
use World\Entity\Outlook\Factory\Exceptions\NotFindNeededOutlookClassException;
use World\Entity\Outlook\OutlookInterface;

interface OutlookFactoryInterface
{
    /**
     * @param EntityInterface $entity
     * @return OutlookInterface
     * @throws NotFindNeededOutlookClassException
     */
    public function create(EntityInterface $entity) : OutlookInterface;
}