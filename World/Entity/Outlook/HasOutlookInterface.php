<?php

namespace World\Entity\Outlook;

use World\Entity\EntityInterface;

interface HasOutlookInterface
{
    /**
     * @param EntityInterface $entity
     *
     * Human can look and interact with another entities
     */
    public function setLook(EntityInterface $entity) : void;
}