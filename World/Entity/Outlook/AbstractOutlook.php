<?php

namespace World\Entity\Outlook;

use World\Actions\ActionInterface;
use World\Entity\EntityInterface;

abstract class AbstractOutlook implements OutlookInterface
{
    /**
     * @var EntityInterface
     */
    protected $entity;

    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    abstract public function delegateAction(ActionInterface $action): ?ActionInterface;

    /**
     * @param null|EntityInterface $entity
     */
    public function __construct(EntityInterface $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getEntityPrefix(): string
    {
        return $this->entity->getPrefix();
    }
}