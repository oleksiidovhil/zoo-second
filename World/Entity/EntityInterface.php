<?php

namespace World\Entity;

use World\Actions\ActionInterface;

/**
 * Interface EntityInterface
 * @package World\Entity
 *
 * This is may to be their purpose carry information about hisself
 */

interface EntityInterface
{
    /**
     * @return ActionInterface
     */
    public function getAction() : ActionInterface;

    /**
     * @return string small information about entity
     */
    public function getPrefix() : string ;
}