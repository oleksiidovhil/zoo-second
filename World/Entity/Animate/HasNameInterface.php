<?php

namespace World\Entity\Animate;

interface HasNameInterface
{
    /**
     * HasNameInterface constructor.
     * @param string $name
     */
    public function __construct(string $name);

    /**
     * @return string
     */
    public function getName() : string ;
}