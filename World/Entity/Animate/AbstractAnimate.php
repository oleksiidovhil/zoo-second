<?php

namespace World\Entity\Animate;

use World\Actions\ActionInterface;
use World\Actions\InitAction;
use World\Entity\Animate\ReactionDriver\Exceptions\NotSetReactionDriver;
use World\Entity\Animate\ReactionDriver\ReactionDriverInterface;

/**
 * Class AbstractAnimate
 * @package World\Entity\Animate
 *
 * It can be animals, birds, or Human.
 * All they have possibility read actions another entities or set his action
 *
 * So as not to overload the constructor use Builder Pattern
 */

abstract class AbstractAnimate implements AnimateInterface
{
    /**
     * @var ActionInterface
     */
    protected $action;

    /**
     * @var ReactionDriverInterface
     */
    protected $driver;

    /**
     * @var string
     */
    protected $name;

    /**
     * AbstractAnimate constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param ReactionDriverInterface $driver
     * @return AnimateInterface
     */
    public function setReactionDriver(ReactionDriverInterface $driver) : AnimateInterface
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return ActionInterface
     */
    public function getAction(): ActionInterface
    {
        return $this->action;
    }

    /**
     * @param ActionInterface $action
     *
     * animate person have possibility get action and change his action depending on get action
     * @throws NotSetReactionDriver
     */
    public function readAction(ActionInterface $action): void
    {
        if (! isset($this->driver) ) throw  new NotSetReactionDriver();
        $newAction = $this->driver->read($action);
        if (isset($newAction) && ! is_null($newAction)) $this->setAction($newAction);
    }

    /**
     * every Reaction reader need to have handler InitAction;
     * this is set default action to animals
     * this function must be called after creation all entities of Animate
     */
    public function heIsAlive()
    {
        $this->readAction(new InitAction());
    }

    /**
     * @param $action ActionInterface
     * @return void
     *
     * only person have possibility set what he doing right now
     */
    protected function setAction(ActionInterface $action) : void
    {
        $action->setPrefix($this->getPrefix());
        $this->action = $action;
    }

    abstract public function getPrefix() : string ;

    /**
     *  Cloning is prohibited by law
     */
    final private function __clone(){}
}