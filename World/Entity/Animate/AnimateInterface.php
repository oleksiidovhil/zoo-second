<?php

namespace World\Entity\Animate;

use World\Entity\Animate\ReactionDriver\HasReactionInterface;
use World\Entity\EntityInterface;

/**
 * Interface AnimateInterface
 * @package World\Entity\Animate
 *
 * this is a animals, humans ect.
 * they have the opportunity doing something now, and read what do ( what are is) other entities
 */

interface AnimateInterface extends EntityInterface, HasReactionInterface, HasNameInterface
{
}