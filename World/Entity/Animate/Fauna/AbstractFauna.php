<?php

namespace  World\Entity\Animate\Fauna;

use World\Actions\ActionInterface;
use World\Entity\Animate\AbstractAnimate;

abstract class AbstractFauna extends AbstractAnimate
{
    /**
     * @param ActionInterface $action
     *
     * set what do now Fauna exemplar and send this to reaction driver
     */
    protected function setAction(ActionInterface $action): void
    {
        parent::setAction($action);
    }
}