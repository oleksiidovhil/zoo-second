<?php

namespace World\Entity\Animate\Fauna\FaunaReactionDriver;

use World\Actions\ActionInterface;
use World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem\DriverItemInterface;
use World\Entity\Animate\ReactionDriver\ReactionDriverInterface;

interface FaunaReactionDriverInterface extends ReactionDriverInterface
{
    /**
     * @param DriverItemInterface $driverItem
     * @return mixed
     */
    public function addDriverItem(DriverItemInterface $driverItem) : FaunaReactionDriverInterface;

    /**
     * @param ActionInterface $action
     * @return ActionInterface
     */
    public function getSpecificAction(ActionInterface $action) : ?ActionInterface;
}
