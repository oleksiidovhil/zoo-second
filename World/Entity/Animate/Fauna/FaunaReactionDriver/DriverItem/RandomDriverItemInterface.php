<?php

namespace World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem;

use World\Actions\ActionInterface;

/**
 * Interface RandomDriverItemInterface
 * @package World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem
 *
 * implements what Fauna may be do in free time
 */
interface RandomDriverItemInterface extends DriverItemInterface
{
    /**
     * @param ActionInterface $action
     */
    public function addPossibleAction(ActionInterface $action) : void;
}