<?php

namespace World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem;

use World\Actions\ActionInterface;

interface DriverItemInterface
{
    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function getReaction(ActionInterface $action) : ?ActionInterface;
}