<?php

namespace World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem;

use World\Actions\ActionInterface;
use World\Actions\InitAction;
use World\Actions\Sit;

/**
 * Class InitDriverItem
 * @package World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem
 *
 * this is default item with default Action
 * All Fauna need to have this driver or set another default driver.
 * If this will not be done, human when came to cage does not see anything
 */
class InitDriverItem implements DriverItemInterface
{
    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function getReaction(ActionInterface $action): ?ActionInterface
    {
        if ($action instanceof InitAction) {
            return new Sit();
        }
        return null;
    }
}