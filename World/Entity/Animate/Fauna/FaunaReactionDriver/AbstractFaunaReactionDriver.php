<?php

namespace World\Entity\Animate\Fauna\FaunaReactionDriver;

use World\Actions\ActionInterface;
use World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem\DriverItemInterface;
use World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem\InitDriverItem;

abstract class AbstractFaunaReactionDriver implements FaunaReactionDriverInterface
{
    /**
     * @var array DriverItemInterface's
     */
    protected $driverItems = [];

    /**
     * AbstractFaunaReactionDriver constructor.
     *
     * default constructor witch have default InitDriverItem
     * overwrite constructor if you need to Fauna exemplar have another default Action
     */
    public function __construct()
    {
        $this->addDriverItem(new InitDriverItem());
    }

    /**
     * @param DriverItemInterface $driverItem
     * @return FaunaReactionDriverInterface
     */
    public function addDriverItem(DriverItemInterface $driverItem) : FaunaReactionDriverInterface
    {
        $this->driverItems[] = $driverItem;
        return $this;
    }

    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function read(ActionInterface $action): ?ActionInterface
    {
        return $this->getSpecificAction($action);
    }

    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function getSpecificAction(ActionInterface $action): ?ActionInterface
    {
        foreach ($this->driverItems as $driverItem) {
            $reaction = $driverItem->getReaction($action);
            if (! is_null($reaction)) return $reaction;
        }
        return null;
    }
}