<?php

namespace World\Entity\Animate\ReactionDriver\Exceptions;

use World\Exceptions\WorldExceptions;

class NotSetReactionDriver extends WorldExceptions
{
    protected function formatError($context): void
    {
        $this->error = 'Reaction driver need to be set';
    }
}