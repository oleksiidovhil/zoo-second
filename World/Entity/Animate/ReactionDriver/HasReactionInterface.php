<?php

namespace World\Entity\Animate\ReactionDriver;

use World\Actions\ActionInterface;
use World\Entity\Animate\AnimateInterface;
use World\Entity\Animate\ReactionDriver\Exceptions\NotSetReactionDriver;

interface HasReactionInterface
{
    /**
     * @param ReactionDriverInterface $driver
     * @return mixed
     */
    public function setReactionDriver(ReactionDriverInterface $driver) : AnimateInterface;

    /**
     * @param ActionInterface $action
     * @throws NotSetReactionDriver
     */
    public function readAction(ActionInterface $action) : void ;

    /**
     *  function what start life of Animate Persone
     */
    public function heIsAlive();
}