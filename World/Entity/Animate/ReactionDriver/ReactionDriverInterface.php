<?php

namespace World\Entity\Animate\ReactionDriver;

use World\Actions\ActionInterface;

interface ReactionDriverInterface
{
    /**
     * @param ActionInterface $action
     * @return mixed
     */
    public function read(ActionInterface $action) : ?ActionInterface;
}