<?php

namespace World\Entity\Animate\Human;

use World\Actions\ActionInterface;
use World\Entity\Animate\AbstractAnimate;
use World\Entity\Outlook\HasOutlookInterface;
use World\Entity\Outlook\OutlookInterface;
use World\Entity\Outlook\Factory\OutlookFactoryInterface;
use World\Entity\EntityInterface;
use Zoo\Actions\Human\Look;

abstract class AbstractHuman extends AbstractAnimate implements HasOutlookInterface
{
    /**
     * @var OutlookInterface
     */
    protected $outlook;

    /**
     * @var OutlookFactoryInterface
     */
    protected $outlookFactory;

    /**
     * AbstractHuman constructor.
     * @param $name string
     *
     * when Human created need to set his Outlook
     */
    public function __construct(string $name)
    {
        $this->setOutlook();
        $this->checkOutlook();
        parent::__construct($name);
    }

    /**
     * @return void
     */
    abstract function setOutlook();

    /**
     * for generate application Animate persona need have Outlook Fabric
     */
    final protected function checkOutlook()
    {
        if (! isset($this->outlookFactory)) throw new NeedOutlookFactoryException();
    }

    /**
     * @param EntityInterface $entity
     */
    final public function setLook(EntityInterface $entity): void
    {
        $this->action = null;
        $this->outlook = $this->outlookFactory->create($entity);
    }

    /**
     * @param ActionInterface $action
     *
     * set what do now Human & share it to his outlook
     */
    protected function setAction(ActionInterface $action): void
    {
        $this->redriveNewAction($action);
        $view = $this->outlook->delegateAction($action);
        parent::setAction($action);
        $this->driver->read($view);
    }

    /**
     * @param ActionInterface $action
     *
     * send sms with this moment action
     */
    abstract protected function redriveNewAction(ActionInterface $action) : void;

}