<?php

namespace World\Entity\Animate\Human;

use World\Exceptions\WorldExceptions;

class NeedOutlookFactoryException extends WorldExceptions
{
    /**
     * @param $context
     */
    public function formatError($context): void
    {
        $this->error = 'Animate Persona need have Outlook factory';
    }
}