<?php

namespace World\Actions;

/**
 * Interface ActionInterface
 * @package World\Actions
 *
 * Property of Entity, shows what do or are entity
 *
 * Functions getSuffix & getPrefix was added for quick test functional.
 *
 * After i add worked Drivers & DriverItems for Animate persons
 * in this interface will be added function getCreator() witch link on creator ( or Name object, i'm not sure yet )
 */
interface ActionInterface
{
    /**
     * @return mixed
     */
    public function get();

    /**
     * @param string $prefix
     * @return mixed
     */
    public function setPrefix(string $prefix) : void;

    /**
     * @param string $suffix
     * @return mixed
     */
    public function setSuffix(string $suffix) : void;
}