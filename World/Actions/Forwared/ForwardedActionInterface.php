<?php

namespace World\Actions\Forwarded;

use World\Actions\ActionInterface;

/**
 * Interface ForwardedActionInterface
 * @package World\Actions\Forwarded
 *
 * action with effect, some like :
 * get() => wolf is bite
 * getEffect() => bite(strength = 45 )
 */

interface ForwardedActionInterface extends ActionInterface
{
    /**
     * @return mixed
     */
    public function get();

    /**
     * @return mixed
     */
    public function getEffect();
}