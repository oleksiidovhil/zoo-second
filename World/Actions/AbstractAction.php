<?php

namespace World\Actions;

class AbstractAction implements ActionInterface
{
    /**
     * @var string
     */
    protected $text;

    /**
     * AbstractAction constructor.
     * @param $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->text;
    }

    /**
     * @param $prefix string
     * @return void
     */
    public function setPrefix(string $prefix) : void
    {
        $this->text = $prefix . $this->text ;
    }

    /**
     * @param string $suffix
     * @return void
     */
    public function setSuffix(string $suffix) : void
    {
        $this->text .= $suffix;
    }
}