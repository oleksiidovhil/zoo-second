<?php

namespace World\Actions;

class InitAction extends AbstractAction
{
    public function __construct($text = '')
    {
        parent::__construct($text);
    }

    public function get()
    {
        return null;
    }
}