<?php

namespace World\Actions;

class Sit extends AbstractAction
{
    /**
     * AbstractAction constructor.
     * @param $text
     */
    public function __construct($text = 'Is sit')
    {
        parent::__construct($text);
    }
}