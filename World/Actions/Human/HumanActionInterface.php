<?php

namespace World\Actions\Human;

use World\Actions\ActionInterface;

interface HumanActionInterface extends ActionInterface {}