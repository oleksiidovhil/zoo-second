<?php

namespace World\Actions\Human;

use World\Actions\AbstractAction;

abstract class AbstractHumanAction extends AbstractAction implements HumanActionInterface
{

}