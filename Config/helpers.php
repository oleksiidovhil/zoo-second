<?php


if (! function_exists('faker')) {
    function faker()
    {
//        if (class_exists('Faker\Factory')) {
            return Faker\Factory::create();
//        } else {
//            return new Helpers\RandomGenerator();
//        }
    }
}


if (! function_exists('factory')) {
    function fauna_factory($fauna, $name = null) {
        if (is_null($name)) $name = faker()->firstName();
        $faunaConfig = Config::$fauna;
        $factory = null;
        switch (true) {
            case in_array($fauna, $faunaConfig['fish']) :
                $factory = new \Zoo\Animate\Factory\FishFactory();
            break;
            case in_array($fauna, $faunaConfig['animals']) :
                $factory = new \Zoo\Animate\Factory\AnimalsFactory();
            break;
            case in_array($fauna, $faunaConfig['birds']) :
                $factory = new \Zoo\Animate\Factory\BirdsFactory();
            break;
            default :
                throw new Exception('not found Fauna Exemplar');
            break;
        }
        return $factory->createFaunaExemplar($fauna, $name);
    }
}