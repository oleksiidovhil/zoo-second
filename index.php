<?php

require './vendor/autoload.php';

use Zoo\Human\ReactionDriver\Mobile;
use Zoo\Inanimate\HelloZoo;
use Zoo\Inanimate\ByeZoo;

$human = new \Zoo\Human\Human('Sergey');
$human->setReactionDriver(new Mobile())->heIsAlive();

$plate = new HelloZoo();
$human->setLook($plate);
$human->look();

$wolf = fauna_factory('wolf');
$fish = fauna_factory('fish');
$tomtit = fauna_factory('tomtit');


$human->setLook($wolf);
$human->look();
$human->look();
$human->look();

$human->setLook($fish);
$human->look();
$human->look();

$human->setLook($tomtit);
$human->look();
$human->look();

$plate = new ByeZoo();
$human->setLook($plate);
$human->look();

//var_dump($human);