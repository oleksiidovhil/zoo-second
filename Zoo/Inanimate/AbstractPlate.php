<?php

namespace Zoo\Inanimate;

use World\Actions\ActionInterface;
use World\Entity\Inanimate\AbstractInanimate;

abstract class AbstractPlate extends AbstractInanimate
{
    /**
     * @return ActionInterface
     */
    public function getAction(): ActionInterface
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return "Plate ";
    }
}