<?php

namespace Zoo\Inanimate;

use Zoo\Actions\Plate;

class HelloZoo extends AbstractPlate
{
    /**
     * HelloZoo constructor.
     */
    public function __construct()
    {
        parent::__construct(new Plate('Hello in Zoo'));
    }

}