<?php

namespace Zoo\Inanimate;

use Zoo\Actions\Plate;

class ByeZoo extends AbstractPlate
{
    /**
     * ByeZoo constructor.
     */
    public function __construct()
    {
        parent::__construct(new Plate('Thank You. Good Luck!'));
    }
}