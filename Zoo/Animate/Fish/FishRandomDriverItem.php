<?php

namespace Zoo\Animate\Fish;

use Zoo\Actions\Animate\EatsAWorm;
use Zoo\Actions\Animate\Swim;
use Zoo\Animate\RandomDriverItem;

class FishRandomDriverItem extends RandomDriverItem
{
    /**
     * @var array
     */
    protected $possibleActions = [
        Swim::class => 'is swimming',
        EatsAWorm::class => 'eats a worm'
    ];
}