<?php

namespace Zoo\Animate\Fish;

use World\Entity\Animate\Fauna\AbstractFauna;

 class Fish extends AbstractFauna
{
     /**
      * @return string
      */
     public function getPrefix(): string
     {
         return 'Fish ' . $this->name . ' ';
     }
}