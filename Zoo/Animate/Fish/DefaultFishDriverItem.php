<?php

namespace Zoo\Animate\Fish;

use World\Actions\ActionInterface;
use World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem\DriverItemInterface;
use Zoo\Actions\Animate\Swim;

class DefaultFishDriverItem implements DriverItemInterface
{
    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function getReaction(ActionInterface $action) : ?ActionInterface
    {
        return new Swim('is swimming');
    }
}