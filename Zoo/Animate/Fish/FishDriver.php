<?php

namespace Zoo\Animate\Fish;

use World\Entity\Animate\Fauna\FaunaReactionDriver\AbstractFaunaReactionDriver;

class FishDriver extends AbstractFaunaReactionDriver
{
    /**
     * FishDriver constructor.
     */
    public function __construct()
    {
        $this->addDriverItem(new DefaultFishDriverItem());
    }
}