<?php

namespace Zoo\Animate\Factory;

use World\Entity\Animate\AnimateInterface;

abstract class AbstractFaunaFactory
{
    /**
     * @param $faunaItem
     * @param $name
     * @return AnimateInterface
     */
    abstract public function createFaunaExemplar($faunaItem, $name) : AnimateInterface;
}