<?php

namespace Zoo\Animate\Factory;

use World\Entity\Animate\AnimateInterface;
use Zoo\Animate\Birds\BirdsRandomDriverItem;
use Zoo\Animate\Birds\Tomtit\Tomtit;
use Zoo\Animate\FaunaReactionDriver;

class BirdsFactory extends AbstractFaunaFactory
{
    protected $config = [
        'tomtit' => Tomtit::class
    ];

    /**
     * @param $faunaItem
     * @param $name
     * @return AnimateInterface
     */
    public function createFaunaExemplar($faunaItem, $name): AnimateInterface
    {
        $bird = new $this->config[$faunaItem]($name);
        $reactionDriver = new FaunaReactionDriver();
        $reactionDriver->addDriverItem(new BirdsRandomDriverItem());
        $bird->setReactionDriver($reactionDriver);
        $bird->heIsALive();
        return $bird;
    }
}