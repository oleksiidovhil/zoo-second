<?php

namespace Zoo\Animate\Factory;

use World\Entity\Animate\AnimateInterface;
use Zoo\Animate\Animals\AnimalRandomDriverItem;
use Zoo\Animate\Animals\Wolf\Wolf;
use Zoo\Animate\FaunaReactionDriver;

class AnimalsFactory extends AbstractFaunaFactory
{
    protected $config = [
        'wolf' => Wolf::class
    ];

    /**
     * @param $faunaItem
     * @param $name
     * @return AnimateInterface
     */
    public function createFaunaExemplar($faunaItem, $name): AnimateInterface
    {
        $animal = new $this->config[$faunaItem]($name);
        $reactionDriver = new FaunaReactionDriver();
        $reactionDriver->addDriverItem(new AnimalRandomDriverItem());
        $animal->setReactionDriver($reactionDriver);
        $animal->heIsALive();
        return $animal;
    }
}