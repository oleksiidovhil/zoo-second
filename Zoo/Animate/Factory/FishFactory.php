<?php

namespace Zoo\Animate\Factory;

use World\Entity\Animate\AnimateInterface;
use Zoo\Animate\FaunaReactionDriver;
use Zoo\Animate\Fish\Fish;
use Zoo\Animate\Fish\FishRandomDriverItem;

class FishFactory extends AbstractFaunaFactory
{
    protected $config = [
        'fish' => Fish::class
    ];

    /**
     * @param $faunaItem
     * @param $name
     * @return AnimateInterface
     */
    public function createFaunaExemplar($faunaItem, $name): AnimateInterface
    {
        $fish = new $this->config[$faunaItem]($name);
        $reactionDriver = new FaunaReactionDriver();
        $reactionDriver->addDriverItem(new FishRandomDriverItem());
        $fish->setReactionDriver($reactionDriver);
        $fish->heIsALive();
        return $fish;
    }
}