<?php

namespace Zoo\Animate\Animals;

use Zoo\Actions\Animate\Run;
use Zoo\Actions\Animate\Walk;
use Zoo\Animate\RandomDriverItem;

class AnimalRandomDriverItem extends RandomDriverItem
{
    /**
     * @var $possibleActions array
     *
     * All $possibleActions for animals
     */
    protected $possibleActions = [
        Run::class => 'is running',
        Walk::class => 'is walking'
    ];
}
