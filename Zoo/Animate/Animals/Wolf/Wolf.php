<?php

namespace Zoo\Animate\Animals\Wolf;

use World\Entity\Animate\Fauna\AbstractFauna;

class Wolf extends AbstractFauna
{
    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return 'Wolf ' . $this->name . '';
    }
}