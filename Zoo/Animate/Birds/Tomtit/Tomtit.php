<?php

namespace Zoo\Animate\Birds\Tomtit;

use World\Entity\Animate\Fauna\AbstractFauna;

class Tomtit extends AbstractFauna
{
    public function getPrefix(): string
    {
        return 'Tomtit' . $this->name . ' ';
    }
}