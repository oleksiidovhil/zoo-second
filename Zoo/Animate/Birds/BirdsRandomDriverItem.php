<?php

namespace Zoo\Animate\Birds;

use Zoo\Actions\Animate\Fly;
use Zoo\Actions\Animate\Run;
use Zoo\Actions\Animate\Walk;
use Zoo\Animate\RandomDriverItem;

class BirdsRandomDriverItem extends RandomDriverItem
{
    /**
     * @var $possibleActions array All posible Actions for birds
     */
    protected $possibleActions = [
        Walk::class => 'is walking',
        Fly::class => 'is Flying',
        Run::class => 'is Running'
    ];
}