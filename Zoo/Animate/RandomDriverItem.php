<?php

namespace Zoo\Animate;

use World\Actions\ActionInterface;
use World\Entity\Animate\Fauna\FaunaReactionDriver\DriverItem\RandomDriverItemInterface;

/**
 * Class RandomDriverItem
 * @package Zoo\Animate\Animals
 *
 * this class determines what the animal will do if all before drivers return null
 *
 *
 *
 * In the original, the actions of the animal must be random,
 * but, that would get all possible variants i created stack used actions
 *
 */

class RandomDriverItem implements RandomDriverItemInterface
{
    /**
     * @var array
     */
    protected $possibleActions = [];

    /**
     * @var array
     */
    protected $usableActions = [];

    /**
     * @param ActionInterface $action
     * @return null|ActionInterface]
     *
     * I will certainly rewrite this to a normal factory))
     */
    public function getReaction(ActionInterface $action): ?ActionInterface
    {
        foreach ($this->possibleActions as $possibleAction => $value)
        {
            if (! in_array($possibleAction , $this->usableActions)) {
                $this->usableActions[] = $possibleAction;
                return new $possibleAction($value);
            }
        }
        $this->usableActions = [];
        reset($this->possibleActions);
        $possibleAction = key($this->possibleActions);
        $this->usableActions[] = $possibleAction;
        return new $possibleAction($this->possibleActions[$possibleAction]);
    }

    /**
     * @param ActionInterface $action
     */
    public function addPossibleAction(ActionInterface $action): void
    {
        $this->possibleActions[get_class($action)] = $action->get();
    }


}