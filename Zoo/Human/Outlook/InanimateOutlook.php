<?php

namespace Zoo\Human\Outlook;

use World\Actions\ActionInterface;
use World\Entity\Outlook\AbstractOutlook;

class InanimateOutlook extends AbstractOutlook
{
    public function delegateAction(ActionInterface $action): ?ActionInterface
    {
        return $this->entity->getAction();
    }
}