<?php

namespace Zoo\Human\Outlook;

use World\Entity\Animate\AnimateInterface;
use World\Entity\EntityInterface;
use World\Entity\Inanimate\InanimateInterface;
use World\Entity\Outlook\Factory\Exceptions\NotFindNeededOutlookClassException;
use World\Entity\Outlook\Factory\OutlookFactoryInterface;
use World\Entity\Outlook\OutlookInterface;

class HumanOutlookFactory implements OutlookFactoryInterface
{
    /**
     * @var array
     */
    protected $config = [
        AnimateInterface::class => AnimateOutlook::class,
        InanimateInterface::class => InanimateOutlook::class
    ];

    /**
     * @param EntityInterface $entity
     * @return OutlookInterface
     * @throws NotFindNeededOutlookClassException
     */
    public function create(EntityInterface $entity): OutlookInterface
    {
        foreach ($this->config as $entityInterface => $outlookClass) {
            if ($entity instanceof $entityInterface) return new $outlookClass($entity);
        }
        throw new NotFindNeededOutlookClassException($entity);
    }
}