<?php

namespace Zoo\Human\Outlook;

use World\Actions\ActionInterface;
use World\Entity\Animate\AnimateInterface;
use World\Entity\Outlook\AbstractOutlook;

class AnimateOutlook extends AbstractOutlook
{
    /**
     * @var AnimateInterface
     */
    protected $entity;

    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function delegateAction(ActionInterface $action): ?ActionInterface
    {
        $this->entity->readAction($action);
        return $this->entity->getAction();
    }

}