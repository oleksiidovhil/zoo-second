<?php

namespace Zoo\Human\ReactionDriver;

use World\Actions\ActionInterface;
use World\Entity\Animate\ReactionDriver\ReactionDriverInterface;

class Mobile implements ReactionDriverInterface
{
    /**
     * @param ActionInterface $action
     * @return null|ActionInterface
     */
    public function read(ActionInterface $action) : ?ActionInterface
    {
        $this->sendSms($action->get());
        return null;
    }

    /**
     * @param string $message
     */
    protected function sendSms(?string $message)
    {
        print $message . "\xA";
    }
}