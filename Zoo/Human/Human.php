<?php

namespace Zoo\Human;

use World\Actions\ActionInterface;
use World\Entity\Animate\Human\AbstractHuman;
use Zoo\Actions\Human\Look;
use Zoo\Human\Outlook\HumanOutlookFactory;

class Human extends AbstractHuman
{
    /**
     * generate outlook for human
     * @return void
     */
    function setOutlook()
    {
        $this->outlookFactory = new HumanOutlookFactory();
    }

    /**
     * @param ActionInterface $action
     *
     * This human send sms every time when some happened
     */
    public function readAction(ActionInterface $action): void
    {
        $this->driver->read($action);
    }

    /**
     * @param ActionInterface $action
     *
     * if action has not been changed we give their to driver
     */
    protected function redriveNewAction(ActionInterface $action) : void
    {
        $thisActionClass = get_class($this->action);
        if ($action instanceof $thisActionClass) return;
        $action->setPrefix($this->getPrefix());
        $action->setSuffix($this->outlook->getEntityPrefix());
        $this->driver->read($action);
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return 'I\'m ';
    }

    /**
     *
     */
    public function look()
    {
        $this->setAction(new Look('Looking at '));
    }

}